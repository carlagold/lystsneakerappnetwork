//
//  LystNetworkingTests.swift
//  LystNetworkingTests
//
//  Created by Carla on 2017/07/16.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import XCTest
@testable import LystNetworking

var sessionUnderTest: URLSession!

class LystNetworkingTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        sessionUnderTest = URLSession(configuration: URLSessionConfiguration.default)

        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        sessionUnderTest = nil
        
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testStringFromHttpParameters() {
        
        var parameters : [String: Any] = [:]
        parameters["pre_product_type"] = "Shoes"
        parameters["pre_category"] = "wedges"
        parameters["pre_gender"] = "F"
        
        let parameterString = parameters.stringFromHttpParameters()
        
        
        XCTAssertEqual(parameterString, "pre_product_type=Shoes&pre_category=wedges&pre_gender=F", "Parameter string construction incorrect")

    }
    
    func testStringFromHttpParametersWithMultipleCategories() {
        
        var parameters : [String: Any] = [:]
        parameters["pre_product_type"] = "Shoes"
        parameters["pre_category"] = ["wedges", "boots"]
        parameters["pre_gender"] = "F"
        
        let parameterString = parameters.stringFromHttpParameters()
        
        
        XCTAssertEqual(parameterString, "pre_product_type=Shoes&pre_category=wedges&pre_category=boots&pre_gender=F", "Parameter string construction incorrect")


    }
    
    // Asynchronous test: success fast, failure slow
    func testValidCallToServerGetsHTTPStatusCode200() {
        // given
        let url = URL(string: "https://api.lyst.com/rest_api/components/feeds/?pre_gender=F&pre_category=boots")
        // 1
        let promise = expectation(description: "Status code: 200")
        
        // when
        let dataTask = sessionUnderTest.dataTask(with: url!) { data, response, error in
            // then
            if let error = error {
                XCTFail("Error: \(error.localizedDescription)")
                return
            } else if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                if statusCode == 200 {
                    // 2
                    promise.fulfill()
                } else {
                    XCTFail("Status code: \(statusCode)")
                }
            }
        }
        dataTask.resume()
        // 3
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    // Asynchronous test: faster fail
    func testCallToServerCompletes() {
        // given
        let url = URL(string: "https://api.lyst.com/rest_api/components/feeds/?pre_gender=F&pre_category=boots")
        // 1
        let promise = expectation(description: "Completion handler invoked")
        var statusCode: Int?
        var responseError: Error?
        
        // when
        let dataTask = sessionUnderTest.dataTask(with: url!) { data, response, error in
            statusCode = (response as? HTTPURLResponse)?.statusCode
            responseError = error
            // 2
            promise.fulfill()
        }
        dataTask.resume()
        // 3
        waitForExpectations(timeout: 5, handler: nil)
        
        // then
        XCTAssertNil(responseError)
        XCTAssertEqual(statusCode, 200)
    }

    
}
