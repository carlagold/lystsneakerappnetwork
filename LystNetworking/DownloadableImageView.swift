//
//  ImageDownloader.swift
//  Lyst Sneaker App
//
//  Created by Carla on 2017/07/16.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import UIKit

public class DownloadableImageView : UIImageView {
    var errorMessage = ""
    var session = URLSession(configuration: .default)
    var downloadTask: URLSessionDownloadTask?
}

public extension DownloadableImageView {
    
    //MARK: Publc
    public func download(remoteURL: URL, localURL: URL, completion: @escaping ((UIImage?) -> ())) {
        self.downloadImage(remoteURL: remoteURL,
                           localURL: localURL,
                           completion : { image in
                            
                            DispatchQueue.main.async(execute: { () -> Void in
                                completion(image)
                            })
                            
            }
        )
    }
    
    
    public func cancelDownload() {
        self.downloadTask?.cancel()
        self.layer.removeAllAnimations()
        self.image = nil
    }
    
    //MARK: Private
    private func downloadImage(remoteURL: URL, localURL: URL, completion: @escaping ((UIImage?)-> ())) {
        
        if let image = retrieveFromFileLocation(localURL) {
            completion(image)
            return
        }
        
        retrieveFromRemoteLocation(remoteURL, localURL: localURL) { image in
            completion(image)
        }
        
    }
    
    private func retrieveFromFileLocation(_ localURL: URL) -> UIImage? {
        if let data = try? Data(contentsOf: localURL) {
            if let image = UIImage(data: data) {
                return image
            }
        }
        
        return nil
    }
    
    private func retrieveFromRemoteLocation(_ remoteURL: URL, localURL: URL, completion: @escaping ((UIImage?)-> ())) {
        downloadTask = session.downloadTask(with: remoteURL, completionHandler: { (location, response, error) in
            if let error = error {
                
                self.errorMessage += "DataTask error: " + error.localizedDescription + "\n"
                completion(nil)
                
            } else if let location = location {
                
                let data = try? Data(contentsOf: location)
                try? data?.write(to: localURL)
                let image = UIImage(data: data!)
                completion(image)
                
            } else {
                completion(nil)
            }
        })
        
        downloadTask?.resume()
    }
    
    
}
