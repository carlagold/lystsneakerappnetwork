//
//  Category.swift
//  Lyst Sneaker App
//
//  Created by Carla on 2017/07/15.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import Foundation
import UIKit

public class CategoryFilter {
    public var label : String?
    public var value : String?
    public var selected : Bool = false
    
    public init(value: String, label: String) {
        self.value = value
        self.label = label
    }
}
