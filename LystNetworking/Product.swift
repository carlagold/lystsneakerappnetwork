//
//  Model.swift
//  Lyst Sneaker App
//
//  Created by Carla on 2017/07/15.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import Foundation
import UIKit

public class Product {
    //We need to show the image, title and price of each product shown on the screen.

    public var title: String
    public var imageDownloadURL : URL
    public var imageLocalURL : URL
    public var price : String
    public var image : UIImage?
    
    public init(title: String, price: String, imageURL: String) {
        self.title = title
        self.price = price
        self.imageDownloadURL = URL(string: imageURL)!
        let fileName = imageDownloadURL.lastPathComponent
        let tmpURL = URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
        
        self.imageLocalURL = tmpURL.appendingPathComponent(fileName)
        
    }

}
