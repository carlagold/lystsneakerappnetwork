//
//  NetworkManager.swift
//  Lyst Sneaker App
//
//  Created by Carla on 2017/07/15.
//  Copyright © 2017 Goldsting. All rights reserved.
//

import Foundation

public class NetworkManager {
    typealias JSONDictionary = [String: Any]
    let baseURL = "https://api.lyst.com/rest_api"
    var errorMessage = ""
    var defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    
    enum HTTPMethod : String {
        case Post = "POST"
        case Get = "GET"
    }
    
    //MARK: - Public API
    public init() {
        
    }
    
    public func fetchProducts(gender: String?, categories: [String]?, callback: @escaping (([Product]?, String) -> Void)) {
        
        
        //1. parameters
        var parameters : [String: Any] = ["pre_product_type" : "Shoes"]
        
        if (categories != nil && categories!.count > 0) {
            parameters["pre_category"] = categories!
        }
        
        if let gender = gender {
            parameters["pre_gender"] = gender
        }
        
        
        //2. componenents
        let components = "/components/feeds/"
        
        //3. createRequest
        if let request = self.buildRequest(method: .Get, parameters: parameters, components: components) {
            
            //4. perform request
            self.makeRequest(request: request) { (response, data, error) in
                
                var products : [Product]?
                
                if let json = self.parseDataToJSON(data) {
                    products = self.parseProducts(json: json);
                }
                
                DispatchQueue.main.async {
                    callback(products, self.errorMessage)
                }
            }
        } else {
            callback(nil, self.errorMessage)
        }
    }
    
    
    public func fetchCategories(gender: String, callback: @escaping (([CategoryFilter]?, String) -> Void)) {
        
        //1. parameters
        let parameters : [String: String] = ["pre_gender" : gender,
                                             "pre_product_type" : "Shoes",
                                             "filter_type" : "category"]
        
        //2. components
        let components = "/components/filter_options/"
        
        //3. create request
        if let request = self.buildRequest(method: .Get, parameters: parameters, components: components) {
            
            //4. perform request
            self.makeRequest(request: request) { (response, data, error) in
                
                var categories : [CategoryFilter]?
                
                if let json = self.parseDataToJSON(data) {
                    categories = self.parseCategories(json: json);
                }
                
                DispatchQueue.main.async {
                    callback(categories, self.errorMessage)
                }
            }
        } else {
            callback(nil, self.errorMessage)
        }
    }
}


//MARK: - Private
extension NetworkManager {
    
    fileprivate func makeRequest(request: URLRequest, completion: @escaping (HTTPURLResponse?, Data?, String) -> ()) {
        dataTask?.cancel() //cancels existing data task if a task already exists so we can reuse for new query
        
        dataTask = defaultSession.dataTask(with: request) { data, response, error in
            
            defer { self.dataTask = nil } //clean up. sets data task to nil after return
            
            if let error = error {
                
                self.errorMessage += "DataTask error: " + error.localizedDescription + "\n"
                
                completion(nil, nil, self.errorMessage)
                
            } else if let data = data,
                let response = response as? HTTPURLResponse,
                response.statusCode == 200 {
                
                completion(response, data, self.errorMessage)
                
            } else {
                
                completion(nil, nil, self.errorMessage)
                
            }
        }
        
        dataTask?.resume() //starts the task
    }
    
    fileprivate func buildRequest(method: HTTPMethod,
                                  parameters: [String: Any],
                                  components: String?) -> URLRequest? {
        
        let urlString = self.baseURL + components!;
        
        if var urlComponents = URLComponents(string: urlString ){
            
            //3. Parameters
            let parameterString = parameters.stringFromHttpParameters()
            urlComponents.query = parameterString
            
            guard let url = urlComponents.url else { return nil }
            
            let request = NSMutableURLRequest(url: url)
            request.httpMethod = method.rawValue
            
            //4. Headers
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Accept")
            
            
            return request as URLRequest;
        }
        
        return nil;
    }
    
    
    //MARK: Parsing JSON to model
    fileprivate func parseDataToJSON(_ data: Data?) -> JSONDictionary? {
        
        guard let data = data else {
            return nil
        }
        
        var response: Any?
        
        do {
            response = try JSONSerialization.jsonObject(with: data, options: [])
        } catch let parseError as NSError {
            self.errorMessage += "JSONSerialization error: \(parseError.localizedDescription)\n"
        }
        
        if let response = response as? JSONDictionary {
            return response
        } else {
            self.errorMessage += "Problem parsing dictionary \n"
        }
        
        return nil
    }
    
    //MARK: Parse Products
    fileprivate func parseProducts(json: JSONDictionary) -> [Product] {
        var results : [Product] = []
        if let products = json["products"] as? [JSONDictionary] {
            for dict in products {
                if let product = parseProduct(dict) {
                    results.append(product)
                }
            }
        }
        
        return results
    }
    
    private func parseProduct(_ dict: JSONDictionary?) -> Product? {
        if let dict = dict,
            let imageURL = dict["image"] as? String,
            let price = dict["price"] as? String,
            let title = dict["title"] as? String {
            
            let product = Product(title: title, price: price, imageURL: imageURL)
            return product
        }
        
        return nil
    }
    
    //MARK: Parse Categories
    fileprivate func parseCategories(json: JSONDictionary) -> [CategoryFilter] {
        var results : [CategoryFilter] = []
        if let categories = json["filters"] as? [JSONDictionary] {
            for dict in categories {
                if let category = parseCategory(dict) {
                    results.append(category)
                }
            }
        }
        
        return results
    }
    
    private func parseCategory(_ dict: JSONDictionary?) -> CategoryFilter? {
        if let dict = dict,
            let label = dict["label"] as? String,
            let value = dict["value"] as? String {
            
            let category = CategoryFilter(value: value, label: label)
            return category
        }
        
        return nil
    }
    

}


//MARK: - Extensions
extension String {
    
    func addingPercentEncodingForURLQueryValue() -> String? {
        let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~")
        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
    }
}


extension Dictionary {
    
    func stringFromHttpParameters() -> String {
        
        let parameterArray = self.map { (key, value) -> String in
            let percentEscapedKey = (key as! String).addingPercentEncodingForURLQueryValue()!
            var ret = ""
            if let values = value as? [String] {
                for val in values {
                    let percentEscapedValue = val.addingPercentEncodingForURLQueryValue()!
                    let parameter = "\(percentEscapedKey)=\(percentEscapedValue)";
                    ret = ret != "" ? (ret + "&\(parameter)") : (ret + parameter)
                }
            } else {
                let percentEscapedValue = (value as! String).addingPercentEncodingForURLQueryValue()!
                ret = "\(percentEscapedKey)=\(percentEscapedValue)"
            }
            
            return ret;
            
        }
        
        return parameterArray.joined(separator: "&")
    }
    
}
